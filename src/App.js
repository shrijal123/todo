import React, { Component } from 'react'
import CreateTodo from './containers/CreateTodo'
import Table from './containers/Table'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div>
          <div>
            <div>
              <CreateTodo />
            </div>
            <Table />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
